'use strict'

var express = require('express');
var UserController = require('../controllers/user');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.get('/home', UserController.home);
api.get('/pruebas', UserController.pruebas);
api.post('/register', UserController.saveUser);
api.post('/login',UserController.loginUser);
api.get('/get-logged-user',md_auth.ensureAuth, UserController.getUserWithToken);
api.get('/user/:id',md_auth.ensureAuth,UserController.getUser);
api.post('/user',md_auth.ensureAuth,UserController.findUser);


module.exports = api;
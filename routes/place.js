'use strict'

var express = require('express');
var PlaceController = require('../controllers/place');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.get('/probando-place', md_auth.ensureAuth, PlaceController.probando);
api.post('/register-place', md_auth.ensureAuth, PlaceController.savePlace);
api.post('/update-place/:id?', md_auth.ensureAuth, PlaceController.updatePlace);
api.delete('/place/:id?', md_auth.ensureAuth,PlaceController.deletePlace);
api.get('/place/:id?', PlaceController.getPlace);
api.get('/get-user-places',md_auth.ensureAuth, PlaceController.getPlaces);
api.get('/get-user-places-markers',md_auth.ensureAuth, PlaceController.getPlacesMarkers);
api.post('/get-place-by-position', PlaceController.getPlaceByPosition);


module.exports = api;
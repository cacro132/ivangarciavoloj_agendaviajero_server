'use strict'

var express = require('express');
var GroupController = require('../controllers/group');
var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.post('/new-group', md_auth.ensureAuth, GroupController.saveGroup);
api.get('/groups',md_auth.ensureAuth, GroupController.getUserGroups);
api.get('/group/:id?',md_auth.ensureAuth, GroupController.getUserGroup);
api.put('/update-group/:id',md_auth.ensureAuth, GroupController.updateGroup);

module.exports = api;
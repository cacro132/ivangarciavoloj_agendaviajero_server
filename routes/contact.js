'use strict'

var express = require('express');
var ContactController = require('../controllers/contact');
var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.post('/add-contact', md_auth.ensureAuth, ContactController.saveContact);
api.get('/contacts', md_auth.ensureAuth, ContactController.getContacts);

module.exports = api;
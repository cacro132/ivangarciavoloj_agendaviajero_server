'use strict'

var express = require('express');
var RouteController = require('../controllers/route');

var api = express.Router();
var md_auth = require('../middlewares/authenticated');

api.get('/probando-route', md_auth.ensureAuth, RouteController.probando);
api.post('/register-route',md_auth.ensureAuth, RouteController.saveRoute);
api.post('/register-route-v2',md_auth.ensureAuth, RouteController.saveRouteV2);
api.get('/get-user-routes',md_auth.ensureAuth, RouteController.getRoutes);
api.get('/route/:id?',md_auth.ensureAuth, RouteController.getRoute);
api.delete('/route/:id',md_auth.ensureAuth, RouteController.deleteRoute);


module.exports = api;
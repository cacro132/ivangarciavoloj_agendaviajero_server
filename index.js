'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.PORT || 8080;
var db = process.env.MONGODB_URI || 'mongodb://localhost:27017/app_travelers_agenda';

// Conexión Database
mongoose.Promise = global.Promise;
//mongoose.connect('mongodb://localhost:27017/app_travelers_agenda')
mongoose.connect(db)
.then(() => {
    console.log("Conexión con la BBDD ----> OK ");
    
    // Crear servidor 
    app.listen(port,() => {
        console.log("Conexión con el servidor ----> OK");
    });
    
})
.catch(err => console.log(err));
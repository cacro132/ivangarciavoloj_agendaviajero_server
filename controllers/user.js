'user strict'

var bcrypt = require('bcrypt-nodejs');
var jwt = require('../services/jwt');

var User = require('../models/user');
var Contact = require('../models/contact');

var mongoosePaginate = require('mongoose-pagination');

// Métodos de prueba
function home(req,res){
    res.status(200).send({
        message: 'Hola mundo desde el servidor de NodeJS'
    });
}

function pruebas(req,res) {
    console.log(req.body);
    res.status(200).send({
        message: 'Acción de pruebas en el servidor de NodeJS'
    });
    
}

// Registro
function saveUser(req, res) {
    
    var params = req.body;
    var user = new User();
    
    if(params.name && params.surname && params.nick && params.email && params.password){
        
        user.name = params.name;
        user.surname = params.surname;
        user.nick = params.nick;
        user.email = params.email;
        user.image = null;
        
        // Controlar usuarios duplicados
        User.find({$or:[ {email: user.email.toLowerCase},{email: user.email},{email: user.nick.toLowerCase},{nick: user.nick}]}).exec((err, users) => {
            
               if(err) return res.status(500).send({message:'Error en la eticion de usuarios'});
            
                    if(users && users.length >= 1){
                     
                        if(users.email == params.email) { 
                            
                            return res.status(200).sends({message: 'BADEMAIL'});
                            
                        } 
                        
                        return res.status(200).send({message: 'BADUSR'});
                                    
                    } else {
                                    
                         // Cifra la password y me guarda los datos
                                bcrypt.hash(params.password, null, null, (err, hash) =>{
                                     user.password = hash;
                                     user.save((err, userStored) => {
                                        if(err) return res.status(500).send({message:'Error al guardar el usuario'});

                                         if(userStored) {
                                             res.status(200).send({message:'OK'});
                                         } else {
                                                res.status(404).send({message: 'No se ha registrado el usuario'});
                                        }

                                        });
                                    });
                                    
                                }
                            });                 
        
        
    } else {
        
        res.status(200).send({
           message: 'FIELDS' 
        });
    }
    
}

// Login
function loginUser(req,res){
    
    var params = req.body;
    
    var user = params.user;
    var password = params.password;
    
    User.findOne({$or:[{email: user},{nick:user}]}, (err,user) => {
        
        if(err) return res.status(500).send({message:'Error en la petición'});
        
        if(user) {
            
            bcrypt.compare(password, user.password, (err, check) => {
                
                if(check) {
                    
                    
                    if(params.gettoken){
                        
                        // Generar y devolver token
                        console.log('User: ' + user.nick + "\nEmail: " + user.email);
                        
                        return res.status(200).send({
                            
                            token: jwt.createToken(user)
                            
                        });
                        
                        

                    }else {
                        
                        // Devolver datos de usuario
                        user.password = undefined;
                        return res.status(200).send({user});
                        
                        
                    }
                    
                    
                }else {
                    
                    return res.status(200).send({message:'BADPWD'});
                    
                }
                    
            });
            
        } else {
            
            return res.status(200).send({message:'BADUSR'});
            
        }
        
    });
}

// Conseguir datos del usuario logueado
function getUserWithToken(req,res) {
    
    var userId = req.user.sub;
    
    User.findById(userId, (err,user) =>{
        
        if(err) return res.status(500).send({message: 'Error en la petición'});
        
        if(!user) return res.status(404).send({message: 'El usuario no existe'});
        
        user.password = undefined;
        user.__v = undefined;
        user.role = undefined;
        res.status(200).send({user});
        
        
    });
    
}


//Obtener datos de un usuario por su ID 
function getUser(req,res) {
    
    var userId = req.params.id;
    
    User.findById(userId, (err,user) =>{
        
        if(err) return res.status(500).send({message: 'Error en la petición'});
        
        if(!user) return res.status(404).send({message: 'El usuario no existe'});
        
        user.password = undefined;
        
        return res.status(200).send({user});
        
        
        
    });
    
}

//Obtener datos de un usuario por su ID 
function findUser(req,res) {
    
    var params = req.body;
    
    var nick = params.nick;
    
    
    var page = 1;
    var itemsPerPage
    
    
    User.find({$or: [{'name': { "$regex": nick, "$options": "i" } },{'nick': { "$regex": nick, "$options": "i" } }]}).paginate(page, 10, (err, users, total) =>{
        
        if(err) return res.status(500).send({message: 'Error en la petición'});
        
        if(!users) return res.status(404).send({message: 'El usuario no existe'});
        
        return res.status(200).send(
                {
                    users,
                    total


                });
            

    });
    
}

async function contactsIds(user_id) {
    
    var contacted = await Follow.find({"user": user_id}).select({'_id':0,'_v':0, 'user':0}).exec()
    
        .then((contacts) => {
                
                 var contacts_clean = [];
        
                contacts.forEach((contact) => {

                    contacts_clean.push(contact.contacted);
                });

                return contacts_clean;
        
        })
        .catch((err)=>{
                return handleError(err);
        });
    
    
        return {
        contacted
    }
}



module.exports = {
    
    home,
    pruebas,
	saveUser,
	loginUser,
    getUserWithToken,
    getUser,
    findUser

}
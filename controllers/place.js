'use strict'

var Place = require('../models/place');
var User = require('../models/user');
var Route = require('../models/route');

function probando(req,res) {
    
    res.status(200).send({message: 'Hola desde el controlador de ubicaciones'});
}

function savePlace(req,res) {
    
    var params = req.body;
    
    console.log(params);
    if(!params.name) return res.status(200).send({message: 'Debes enviar un texto'});
    
    var place = new Place();
    
    place.name = params.name;
    place.description = params.description;
    place.score = params.score;
    place.category = params.category;
    place.town = params.town;
    place.province = params.province;
    place.web = params.web;
    place.email = params.email;
    place.phone = params.phone;
    place.favorite = params.favorite;
    place.latitude = params.latitude;
    place.longitude = params.longitude;
    place.user = req.user.sub;

    place.save((err, placeStored) => {
        
        if(err) return res.status(500).send({message:'Error al guardar la ubicación'});
        
        if(!placeStored) return res.status(404).send({message:'La ubicación no ha sido guardada'});
        
        return res.status(200).send({place: placeStored});
        
    });
    
}

function updatePlace(req,res) {
    
    var placeId = req.params.id;
    var update = req.body;
    
    
    Place.findByIdAndUpdate(placeId, update, {new:true}, (err, placeUpdated) => {
        
        if(err) return res.status(500).send({message: 'Error en la petición'});
        
        if(!placeUpdated) return res.status(404).send({message:'No se ha podido actualizar la ubicacion'});
        
        return res.status(200).send({place: placeUpdated});
        
    });
    
}

function deletePlace(req,res) {
    
    var placeId = req.params.id;
    
    Place.find({'user': req.user.sub, '_id': placeId}).remove((err, placeRemoved) => {
        
        if(err) return res.status(500).send({message:'Error al borrar publicacion'});  
           
        if(!placeRemoved) return res.status(404).send({message:'No se ha borrado la ubicación'});
        
        deleteRoutesWithPlace(placeId).then((value) => {
            
            
             return res.status(200).send({message: 'deleted'});
            
            
                                            
        });
       
        
    });
}

//Funcion para borrar rutas donde este la ubicación a borrrar

async function deleteRoutesWithPlace(place_id) {
    
    var deleted = await Route.find({ places: place_id}).remove().exec()
    
    .then((routeRemoved) => {
                
            return routeRemoved;
        
        })
        .catch((err)=>{
                return handleError(err);
        });
    
    return {deleted}
}

function getPlaces(req,res) {
    
    var userId = req.user.sub;
    
    
    Place.find({user: userId}).populate("place").exec((err, places, total) => {
        
        if(err) return res.status(500).send({message:'Error en la petición'});
        
        if(!places) return res.status(404).send({message:'No hay ubicaciones'});
        
        return res.status(200).send({
        
            places
        
        });
        
    });
}

function getPlacesMarkers(req,res) {
    
    var userId = req.user.sub;
    
    Place.find({user: userId}, {'_id':true, 'latitude':true,'longitude':true,'name':true,'description':true}).exec((err, markers) => {
        
   
        if(err) return res.status(500).send({message:'Error en la petición'});
        
        if(!markers) return res.status(404).send({message:'No hay ubicaciones'});
        
        return res.status(200).send({
        
            markers
        
        });
        
        
    });    
}


function getPlace(req,res) {
    
    var placeId = req.params.id;
    
    Place.findById(placeId, (err, place) => {
        
        if(err) return res.status(500).send({message:'Error al devolver la ubicacion'});  
           
        if(!place) return res.status(404).send({message:'No existe la ubicacion'});
        
        place.latitude = undefined;
        place.longitude = undefined;
        place.__v = undefined;
        place.user = undefined;
        
        return res.status(200).send({place});
        
    });
    
}

function getPlaceByPosition(req,res) {
    
    var params = req.body;
    
    var latitude = params.latitude;
    var longitude = params.longitude;
    
    
    Place.findOne({$and: [{latitude: params.latitude},{longitude: params.longitude}]}).exec((err, place) => {
        
        if(err) return res.status(500).send({message:'Error en la petición'});
        
        if(!place) return res.status(404).send({message:'No se ha encontrado la ubicacion'});
        
        return res.status(200).send({
        
            place
        
        });
        
    });
}


module.exports = {
    
    probando,
    savePlace,
    updatePlace,
    deletePlace,
    getPlace,
    getPlaces,
    getPlacesMarkers,
    getPlaceByPosition
}
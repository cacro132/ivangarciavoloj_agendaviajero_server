'use strict'

var path = require('path');
var fs = require('fs');
var moment = require('moment');
var mongoosePaginate = require('mongoose-pagination');
var ObjectId = require('mongodb').ObjectID;

var Place = require('../models/place');
var User = require('../models/user');
var Route = require('../models/route');


function probando(req,res) {
    
    res.status(200).send({message: 'Hola desde el controlador de rutas'});
}

function saveRoute(req,res) {
    
    var userId = req.user.sub;
    var params = req.body;
    var places = params.places;
    var route = new Route();
    
    route.name = params.name;
    var placesObj = new Array();
    route.user = userId;

    
    for(var i = 0; i < places.length; i++) {
        
        console.log('Place number: ' + i+1);
        placesObj.push(new ObjectId(places[i]));
    }
    
    route.places = placesObj;
   
    
    route.save((err, routeStored) => {
        
        if(err) return res.status(500).send({message: 'Error al guardar ruta'});
        
        if(!routeStored) return res.status(404).send({message: ' La ruta no ha sido guardada'});
        
        
        return res.status(200).send({route: routeStored});
        
    });
   
    
}

function saveRouteV2(req,res) {
    
    var userId = req.user.sub;
    var params = req.body;
    var origin = params.origin;
    var name = params.name;
    var destiny = params.destiny;
    var waypoints = params.waypoints;
    var duration = params.duration;
    var distance = params.distance;
    
    var route = new Route();
    
    route.user = userId;
    route.name = name;
    route.origin = new ObjectId(origin);
    route.destiny = new ObjectId(destiny);
    
    var waypoints_list = new Array();
    
    for(var i=0; i< waypoints.length; i++) {
        
        console.log("Waypoint number: " + i);
        waypoints_list.push({
            
            place: new ObjectId(waypoints[i].place),
            motive: waypoints[i].motive,
            duration: waypoints[i].duration,
            distance: waypoints[i].distance
            
        });
    }
    
    route.waypoints = waypoints_list;
    route.duration = duration;
    route.distance = distance;
    
    route.save((err, routeStored) =>{
        
         if(err) return res.status(500).send({message: 'Error al guardar ruta'});
        
        if(!routeStored) return res.status(404).send({message: ' La ruta no ha sido guardada'});
        
        
        return res.status(200).send({route: routeStored});
        
    
    });
    
    
}

function getRoute(req,res){
    
    var routeId = req.params.id;
    
    Route.findById(routeId).populate("route").populate("origin","name latitude longitude description").populate("destiny","name latitude longitude description").populate("waypoints.place","name latitude longitude description").exec((err,route) =>{
        
        if(err) return res.status(500).send({message:'Error en la petición'});
        
        if(!route) return res.status(404).send({message:'No hay rutas'});
        
       return res.status(200).send({route});
      
       
        
    });
    
}

function deleteRoute(req,res) {
    
    var routeId = req.params.id;
    
    Route.find({'user': req.user.sub, '_id': routeId}).remove((err, routeRemoved) => {
        
        if(err) return res.status(500).send({message:'Error al borrar ruta'});  
           
        if(!routeRemoved) return res.status(404).send({message:'No se ha borrado la ruta'});
        
        console.log("deleted");
        return res.status(200).send({message: 'deleted'});
       
        
    });
}

function getRoutes(req,res) {
    
    var userId = req.user.sub;
    
    
    Route.find({user: userId}).populate("route").populate("origin","name description latitude longitude").populate("destiny","name description latitude longitude").populate("waypoints.place","name description latitude longitude").exec((err, routes, total) => {
        
        if(err) return res.status(500).send({message:'Error en la petición'});
        
        if(!routes) return res.status(404).send({message:'No hay rutas'});
        
        return res.status(200).send({
        
            routes
        
        });
        
    });
}

module.exports = {
    probando,
    saveRoute,
    getRoute,
    deleteRoute,
    getRoutes,
    saveRouteV2
}
'use strict'

//var path = require('path');
//var fs = require('fs');
var mongoosePaginate = require('mongoose-pagination');
var ObjectId = require('mongodb').ObjectID;

var Group = require('../models/group');

function saveGroup(req,res) {
    
    var params = req.body;
    var contacts = params.users;
    var group = new Group();
    
    //Convertimos a formato ObjectId las ids obtenidas del cliente
    
    var contacts_array = new Array();
    
    contacts_array.push(req.user.sub);
    
    contacts.forEach((contact) => {
contacts_array.push(new ObjectId(contact));
                });
    
    
    group.name = params.name;
    group.user = req.user.sub;
    group.contacts = contacts_array;
    
    
    group.save((err, groupStored) => {
            
                if(err) return res.status(500).send({message:'Error al guardar el grupo'});

                if(!groupStored) return res.status(404).send({message:'El grupo no se ha guardado'});

                return res.status(200).send({group: groupStored});
            
        }); 
}

function saveFullGroup(req,res) {
    
    var params = req.body;
    var users = params.users;
    var places = params.places;
    var routes = params.routes;
    var group = new Group();
    
    //Convertimos a formato ObjectId las ids obtenidas del cliente
    
    var users_array = new Array();
    var places_array = new Array();
    var routes_array = new Array();
    
    users_array.push(req.user.sub);
    
    users.forEach((user) => {
users_array.push(new ObjectId(user));
                });
    
    places.forEach((place) => {
places_array.push(new ObjectId(place));
                });
    
    routes.forEach((route) => {
routes_array.push(new ObjectId(route));
                });
    
    group.name = params.name;
    group.user = req.user.sub;
    group.users = users_array;
    group.places = places_array;
    group.routes = routes_array;
    
    
    group.save((err, groupStored) => {
            
                if(err) return res.status(500).send({message:'Error al guardar el grupo'});

                if(!groupStored) return res.status(404).send({message:'El grupo no se ha guardado'});

                return res.status(200).send({group:groupStored});
            
        }); 
}

// Edición de datos de usuario
function updateGroup(req,res) {
    
    var groupId = req.params.id;
    var update = req.body;
    
    Group.findByIdAndUpdate(groupId, update, {new:true}, (err, groupUpdated) => {
        
        if(err) return res.status(500).send({message: 'Error en la petición'});
        
        if(!groupUpdated) return res.status(404).send({message:'No se ha podido actualizar el grupo'});
        
        return res.status(200).send({group: groupUpdated});
        
    });
    
}

function getUserGroups(req,res) {
    
    Group.find({contacts: req.user.sub}).exec((err,groups)=>{
        
         if(err) return res.status(500).send({message: 'Error en la petición'});
        
        if(!groups) return res.status(404).send({message: 'El usuario no pertenece a ningun grupo'});
        
        res.status(200).send({groups});
        
    });
    
}

function getUserGroup(req,res) {
    
    var params = req.body;
    var groupId = req.params.id;
    
    Group.findById(groupId)
        .populate("contacts","_id name nick surname")
        .populate("places","_id name town province latitude longitude description").exec((err,contact)=>{
        
        if(err) return res.status(500).send({message:'Error en la petición'});
        
        if(!contact) return res.status(404).send({message: 'El usuario no pertenece a ningun grupo'});
        
        res.status(200).send({
            result:contact
        });
        
    });
    
}


module.exports = {
    
    saveGroup,
    saveFullGroup,
    getUserGroups,
    getUserGroup,
    updateGroup
}
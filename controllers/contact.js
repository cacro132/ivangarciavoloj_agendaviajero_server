'use strict'

//var path = require('path');
//var fs = require('fs');
var mongoosePaginate = require('mongoose-pagination');

var User = require('../models/user');
var Contact = require('../models/contact');

function saveContact(req,res) {
    
    var params = req.body;
    
    var contact = new Contact();
    contact.user = req.user.sub;
    contact.added = params.added;
    
     contact.save((err, contactStored) => {
            
                if(err) return res.status(500).send({message:'Error al guardar el contacto'});

                if(!contactStored) return res.status(404).send({message:'El contacto no se ha guardado'});

                return res.status(200).send({contact:contactStored});
            
        }); 

}

function getContacts(req,res) {
    
    
    Contact.find({"user":req.user.sub}).populate('added', '_id nick name surname').select({'_id':0,'__v':0,'user':0}).paginate(null, null, (err, contacts, total) => {
            
            
            if(err) return res.status(500).send({message: 'Error en la petición'});
            
            if(!contacts)return res.status(404).send({message: 'El usuario no existe'});

        
                return res.status(200).send(
                {
                    contacts,
                    total


                });
            
            });
        
}

module.exports = {
    
    saveContact,
    getContacts
}
'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = '12344321';


exports.createToken = function(user) {
    var payload = {
        sub: user._id,
        name: user.name,
        surname: user.surname,
        nick: user.nick,
        email: user.email,
        image: user.image,
        exp: moment().add(10,'d').unix(),
        iat: moment().unix()
    };
    
    return jwt.encode(payload, secret);
};
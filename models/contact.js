'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ContactSchema = Schema({
    
    user: { type: Schema.ObjectId, ref:'User'},
    added:{ type: Schema.ObjectId, ref:'User'}
    
});

module.exports = mongoose.model('Contact', ContactSchema);
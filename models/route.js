'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RouteSchema = Schema({
    
    name: String,
    origin: {type: Schema.ObjectId, ref:'Place'},
    destiny: {type: Schema.ObjectId, ref:'Place'},
    waypoints:  [
        {
            duration: String,
            distance:String,
            motive: String,
            place: {type: Schema.ObjectId, ref:'Place'}
        }
    ],
    duration: String,
    distance: String,
    user: { type: Schema.ObjectId, ref:'User'}
    
});

module.exports = mongoose.model('Route', RouteSchema);
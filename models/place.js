'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PlaceSchema = Schema({
    
    name: String,
    description: String,
    score: String,
    category: String,
    town: String,
    province: String,
    web: String,
    email: String,
    phone: String,
    favorite: String,
    latitude: String,
    longitude: String,
    privacity: String,
    user: { type: Schema.ObjectId, ref:'User'}
    
});

module.exports = mongoose.model('Place', PlaceSchema);
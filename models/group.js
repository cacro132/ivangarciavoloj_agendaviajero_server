'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GroupSchema = Schema({
    
    name: String,
    user: {type: Schema.ObjectId, ref:'User'},
    contacts: [ {type: Schema.ObjectId, ref:'User'}],
    places:  [ {type: Schema.ObjectId, ref:'Place'}],
    routes:  [ {type: Schema.ObjectId, ref:'Route'}]
    
});

module.exports = mongoose.model('Group', GroupSchema);